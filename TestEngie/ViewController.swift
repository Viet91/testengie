//
//  ViewController.swift
//  TestEngie
//
//  Created by NGUYEN Huu Viet on 07/09/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var consoViewModel = [ConsoViewModel]()
    var collectionView: UICollectionView!
    var collectionViewFlowControl: UICollectionViewFlowLayout!
    
    let barChartView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let titleChart: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Consommation Statistique"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    fileprivate func setupBarChartView() {
        self.view.addSubview(barChartView)
        barChartView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 8).isActive = true
        barChartView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 8).isActive = true
        barChartView.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -16).isActive = true
        barChartView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
        barChartView.backgroundColor = .white
        barChartView.layer.cornerRadius = 8
        barChartView.layer.shadowOpacity = 0.05
        barChartView.layer.shadowRadius = 8
        barChartView.layer.shadowOffset = .init(width: 0, height: 12)
        barChartView.layer.shadowColor = UIColor.black.cgColor
        
        barChartView.addSubview(titleChart)
        titleChart.topAnchor.constraint(equalTo: barChartView.topAnchor).isActive = true
        titleChart.leftAnchor.constraint(equalTo: barChartView.leftAnchor, constant: 8).isActive = true
        titleChart.widthAnchor.constraint(equalTo: barChartView.widthAnchor).isActive = true
        titleChart.heightAnchor.constraint(equalTo: barChartView.heightAnchor, multiplier: 1/4).isActive = true
        
        collectionViewFlowControl = UICollectionViewFlowLayout()
        collectionViewFlowControl.scrollDirection = UICollectionView.ScrollDirection.horizontal
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowControl)
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        barChartView.addSubview(collectionView)
        
        collectionView.bottomAnchor.constraint(equalTo: barChartView.bottomAnchor, constant: -8).isActive = true
        collectionView.leftAnchor.constraint(equalTo: barChartView.leftAnchor, constant: 8).isActive = true
        collectionView.widthAnchor.constraint(equalTo: barChartView.widthAnchor, constant: -16).isActive = true
        collectionView.heightAnchor.constraint(equalTo: barChartView.heightAnchor, multiplier: 3/4).isActive = true
    }
    
    fileprivate func setupNavBar() {
        navigationItem.title = "Engie Consommation"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.rgb(r: 50, g: 199, b: 242)
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.9213467836, green: 0.9355600476, blue: 0.9862597585, alpha: 1)
        setupNavBar()
        setupBarChartView()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(ConsoCell.self, forCellWithReuseIdentifier: "MyCell")
        self.consoViewModel = Service.shared.fetchConsos()?.map({return ConsoViewModel(conso: $0)}) ?? []
        self.collectionView.reloadData()
    }

    func showToast(controller: UIViewController, message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.white
        alert.view.layer.cornerRadius = 15
        controller.present(alert, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
}


extension ViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return consoViewModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! ConsoCell
        cell.consoViewModel = consoViewModel[indexPath.row]
        return cell
    }
}

extension ViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showToast(controller: self, message: "Conso: "+String(consoViewModel[indexPath.row].valeur), seconds: 0.5)
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/20, height: self.barChartView.frame.size.height*3/4)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
    }
}

extension UIColor {
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

