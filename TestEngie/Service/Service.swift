//
//  Service.swift
//  TestEngie
//
//  Created by NGUYEN Huu Viet on 08/09/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import Foundation

let data = "{\"conso\":[{\"date\":\"2018-02\",\"valeur\":990.55},{\"date\":\"2018-01\",\"valeur\":219.95},{\"date\":\"2017-12\",\"valeur\":258.22},{\"date\":\"2017-11\",\"valeur\": 170.93},{\"date\":\"2017-10\",\"valeur\":151.49},{\"date\":\"2017-09\",\"valeur\":114.7},{\"date\":\"2017-08\",\"valeur\":112.28},{\"date\":\"2017-07\",\"valeur\": 108.32},{\"date\":\"2017-06\",\"valeur\":120.3},{\"date\":\"2017-05\",\"valeur\": 148.11},{\"date\":\"2017-04\",\"valeur\":171.78},{\"date\":\"2017-03\",\"valeur\": 217.51},{\"date\":\"2017-02\",\"valeur\":213.97},{\"date\":\"2017-01\",\"valeur\":231.55}]}".data(using: .utf8)


class Service: NSObject {
    static let shared = Service()
    
    func fetchConsos() -> [Conso]? {
        if let data = data {
            do {
                let listConso = try JSONDecoder().decode(ListConso.self, from: data)
                return listConso.conso
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
        }
        return nil
    }
    
}
