//
//  ConsoCell.swift
//  TestEngie
//
//  Created by NGUYEN Huu Viet on 08/09/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import UIKit

class ConsoCell: UICollectionViewCell {
    static let startColor: UIColor = UIColor.rgb(r: 5, g: 221, b: 235)
    static let endColor: UIColor = UIColor.rgb(r: 12, g: 179, b: 247)
    
    var consoViewModel: ConsoViewModel! {
        didSet {
            heightConstraint?.isActive = false
            heightConstraint = consoView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: CGFloat(consoViewModel.ratio))
            heightConstraint?.isActive = true

        }
    }
    var consoView: UIView!
    fileprivate var heightConstraint: NSLayoutConstraint?
    override init(frame: CGRect) {
        super.init(frame: frame)

        consoView = UIViewWithGradientColor(frame: .zero)
        consoView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(consoView)
        
        consoView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        consoView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        consoView.widthAnchor.constraint(equalTo: self.contentView.widthAnchor).isActive = true
        heightConstraint = consoView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor)
        heightConstraint?.isActive = true
        
        self.contentView.backgroundColor = .clear
        self.contentView.layer.cornerRadius = 10
        self.consoView.layer.cornerRadius = 10
        self.contentView.layer.borderWidth = 1
        self.contentView.layer.borderColor = UIColor.rgb(r: 222, g: 222, b: 227).cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

class UIViewWithGradientColor: UIView {
    override func draw(_ rect: CGRect) {
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ConsoCell.startColor.cgColor, ConsoCell.endColor.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
