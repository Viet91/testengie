//
//  ConsoViewModel.swift
//  TestEngie
//
//  Created by NGUYEN Huu Viet on 08/09/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import UIKit


struct ConsoViewModel {
    let valeur: Float
    let ratio: Float
    
    init(conso: Conso) {
        self.valeur = conso.valeur
        ratio = valeur/1200
    }
}
