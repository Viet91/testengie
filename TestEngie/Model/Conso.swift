//
//  Conso.swift
//  TestEngie
//
//  Created by NGUYEN Huu Viet on 07/09/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import Foundation
struct ListConso: Decodable {
    let conso: [Conso]?
}

struct Conso: Decodable {
    let date: String
    let valeur: Float
}
